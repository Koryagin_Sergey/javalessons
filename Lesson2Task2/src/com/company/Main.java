package com.company;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double dis, x1, x2;
        System.out.println("Input a: ");
        double a = Integer.parseInt(in.nextLine());
        System.out.println("Input b: ");
        double b = Integer.parseInt(in.nextLine());
        System.out.println("Input c: ");
        double c = Integer.parseInt(in.nextLine());

        dis = b * b - 4 * a * c;

        if (dis < 0) {
            System.out.println("Не имеет корней");
        } else if (dis == 0) {
            x1 = -b / 2 * a;
            System.out.println("x1 = " + x1);
        } else {
            x1 = (-b + Math.sqrt(dis)) / 2 * a;
            x2 = (-b - Math.sqrt(dis)) / 2 * a;
            System.out.println("x1 = " + x1 + ", " + "x2 = " + x2);
        }


    }
}

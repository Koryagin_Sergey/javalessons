package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Input text1: ");
        String text1 = scan.nextLine();
        System.out.println("Input text2: ");
        String text2 = scan.nextLine();
        System.out.println("Input text3: ");
        String text3 = scan.nextLine();
        int minLength = -1;
        String stringOutput = "";
        String[] textArray = {text1, text2, text3};
        for (int i = 0; i < textArray.length; i++) {
            if (minLength < 0) {
                minLength = textArray[i].length();
                stringOutput = textArray[i];
            }
            if (minLength > textArray[i].length()) {
                minLength = textArray[i].length();
                stringOutput = textArray[i];
            }
        }
        System.out.println("Shortest string: " + stringOutput);
        System.out.println("Only " + minLength + " characters.");


    }
}

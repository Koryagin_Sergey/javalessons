package com.sourceit;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the number of rows: ");
        int numberRows = Integer.parseInt(scan.nextLine());
        String[] textArray = new String[numberRows];
        for (int i = 0; i < textArray.length; i++) {
            System.out.println("Input text" + (i + 1) + ": ");
            textArray[i] = scan.nextLine();
        }
        Arrays.sort(textArray);
        for (int i = 0; i < textArray.length; i++) {
            for (int j = 0; j < textArray.length - 1 - i; j++) {
                if (textArray[j].length() > textArray[j + 1].length()) {
                    String temp = textArray[j];
                    textArray[j] = textArray[j + 1];
                    textArray[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < textArray.length; i++) {
            System.out.println(textArray[i]);
        }
    }
}

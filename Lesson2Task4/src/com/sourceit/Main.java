package com.sourceit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the number of rows: ");
        int numberRows = Integer.parseInt(scan.nextLine());
        String[] textArray = new String[numberRows];
        for (int i = 0; i < textArray.length; i++) {
            System.out.println("Input text" + (i + 1) + ": ");
            textArray[i] = scan.nextLine();
        }
        double totalSize = 0;

        for (int i = 0; i < textArray.length; i++) {
            totalSize = totalSize + textArray[i].length();
        }
        double averageLineSize = totalSize / numberRows;
        for (int i = 0; i < textArray.length; i++) {
            if (textArray[i].length() < averageLineSize) {
                System.out.println("This String length is less than average: " + textArray[i]);
            }

        }


    }
}

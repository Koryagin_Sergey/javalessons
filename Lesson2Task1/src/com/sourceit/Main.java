package com.sourceit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input a: ");
        int a = Integer.parseInt(in.nextLine());
        System.out.println("Input b: ");
        int b = Integer.parseInt(in.nextLine());
        System.out.println("Input c: ");
        int c = Integer.parseInt(in.nextLine());

        if (a < b && a < c) {
            System.out.println(b * b + c * c);
        } else if (b < c && b < a) {
            System.out.println(a * a + c * c);
        } else {
            System.out.println(b * b + a * a);
        }


    }
}
